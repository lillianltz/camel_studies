package replace;

import java.util.Scanner;

public class StringReplace {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Escreva uma frase: ");
		String phrase = sc.nextLine();
		System.out.println(phrase);
		
		String replace = phrase
				.replace('a', '4')
				.replace('e', '3')
				.replace('i', '1')
				.replace('o', '0')
				.replace('u', '5');
				
		
			System.out.println(replace);

	}

}
